# ocp4Power catalog from OCP311

OCP 4.3 for Power comes with an empty application catalog.   
This repository provides script to get application catalog from ocp311 Power into OCP 4.3.  

## How to use it.

- Clone this repository on your bastion node (helpernode)

- run `./install_ocp3_catalog.sh` script
The script will ask you:
  - your OCP4 cluster admin login/passwd
  - redhat login/passwd

```
git clone https://gitlab.com/PSLC/ocp4power_catalog_from_ocp311.git
cd ocp4Power_catalog_from_OCP311
./install_ocp3_catalog.sh
```
