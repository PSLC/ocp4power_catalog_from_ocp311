#!/bin/bash

echo
echo
echo "Login to OCP with admin credential"
echo

oc login  https://127.0.0.1:6443 --insecure-skip-tls-verify=true
oc project openshift

DEST_CRED=admin:$(oc whoami -t)

MASTER_NODE=$(oc get node | awk '$3=="master" { print $1 }' | head -n1)

echo
echo "Step1: Fetching Container Images from Redhat"
echo "============================================="
echo

ssh -t core@$MASTER_NODE "

echo
echo "Login to the cluster"
echo
oc login --token=${DEST_CRED#admin:} https://127.0.0.1:6443 --insecure-skip-tls-verify=true

echo
echo "Login to redhat registry with your redhat account"
echo

podman login registry.redhat.io

skopeo copy --dest-creds $DEST_CRED docker://registry.access.redhat.com/ubi7/nodejs-10:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/nodejs:10
skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/nodejs-6-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/nodejs:6
skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/nodejs-8-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/nodejs:8
skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhoar-nodejs/nodejs-8 docker://image-registry.openshift-image-registry.svc:5000/openshift/nodejs:8-RHOAR
oc tag nodejs:10 nodejs:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/mongodb-36-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/mongodb:3.6
oc tag mongodb:3.6 mongodb:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/httpd-24-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/httpd:2.4
oc tag httpd:2.4 httpd:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/redhat-openjdk-18/openjdk18-openshift:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/java:8
oc tag java:8 java:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/nginx-112-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/nginx:1.12
oc tag nginx:1.12 nginx:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/perl-526-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/perl:5.26
oc tag perl:5.26 perl:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/php-71-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/php:7.1
oc tag php:7.1 php:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/python-27-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/python:2.7
skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/python-36-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/python:3.6
oc tag python:3.6 python:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/ruby-25-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/ruby:2.5
oc tag ruby:2.5 ruby:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/mariadb-102-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/mariadb:10.2
oc tag mariadb:10.2 mariadb:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/openshift3/jenkins-2-rhel7:v3.11 docker://image-registry.openshift-image-registry.svc:5000/openshift/jenkins:2
oc tag jenkins:2 jenkins:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/mysql-57-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/mysql:5.7
oc tag mysql:5.7 mysql:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/rhscl/postgresql-10-rhel7:latest docker://image-registry.openshift-image-registry.svc:5000/openshift/postgresql:10
oc tag postgresql:10 postgresql:latest

skopeo copy --dest-creds $DEST_CRED docker://registry.redhat.io/devtools/go-toolset-rhel7:1.11.5 docker://image-registry.openshift-image-registry.svc:5000/openshift/golang:1.11.5
oc tag golang:1.11.5 golang:latest
"

echo
echo "Step2: Installing Templates"
echo "============================"
echo

cd templates
for template in *.yaml ; do
    oc create -f $template -n openshift
done